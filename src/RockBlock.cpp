//
//  RockBlock.cpp
//  Lab06_Pointers
//
//  Created by Thomas Noya TNOYA001 on 12/10/19.
//

#include "RockBlock.hpp"

RockBlock::RockBlock(float x, float y, float z, float w, float h, float d)
    : SuperBox(x, y, z, w, h, d)
{
    xPos = x;
    yPos = y;
    zPos = z;
    box.setWidth(w);
    box.setHeight(h);
    box.setDepth(d);
}

void RockBlock::draw()
{

    ofPushMatrix();
    ofTranslate(getXPos(), getYPos(), getZPos());
    texture.bind();
    box.draw();
    texture.unbind();
    ofPopMatrix();
    
}

void RockBlock::setTexture(ofImage& _texture)
{
    texture = _texture;
}
