//https://learn.gold.ac.uk/mod/forum/discuss.php?d=240030#p349249 -- my forum post 

#pragma once

#include "ofMain.h"
#include "superBox.h"
#include "positionmarker.h"
#include "TreeBlock.hpp"
#include "Ball.hpp"
#include "flower.h"
#include "RockBlock.hpp"

class ofApp : public ofBaseApp{

	public:
		void setup();
		void update();
		void draw();

		void keyPressed(int key);
		void keyReleased(int key);
		void mouseMoved(int x, int y );
		void mouseDragged(int x, int y, int button);
		void mousePressed(int x, int y, int button);
		void mouseReleased(int x, int y, int button);
		void mouseEntered(int x, int y);
		void mouseExited(int x, int y);
		void windowResized(int w, int h);
		void dragEvent(ofDragInfo dragInfo);
		void gotMessage(ofMessage msg);
		
        ofLight light;
        SuperBox sbox{500,500,0, 20,20,0};
        TreeBlock tree{500, 500, 100, 50, 50, 50};
        Ball ball{500, 500, 0, 50, 50, 0};
        Flower flower{500, 500, 100, 50, 50, 50};
        RockBlock rock{500,500,0, 20,20,0};
        PositionMarker marker{70};
        //std::vector<SuperBox> blocks;
        std::vector <SuperBox*> blocks;
        float blockSize;
        ofImage blockTexture{"grass.jpg"};
        ofImage rockTexture{"rock.jpg"};
        ~ofApp();
};
