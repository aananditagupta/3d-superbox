//
//  TreeBlock.hpp
//  World
//
//  Created by Aanandita Gupta on 10/12/2019.
//

#ifndef TreeBlock_hpp
#define TreeBlock_hpp

#include <stdio.h>

#include "superBox.h"
#include "ofMain.h"

class TreeBlock : public SuperBox {
public:
  TreeBlock(float x, float y, float z, float w, float h, float d);
  virtual void draw(); //
private:
    ofConePrimitive foliage;
    ofCylinderPrimitive trunk;
    ofMaterial foliageMaterial;
    ofMaterial trunkMaterial;
};

#endif /* TreeBlock_hpp */
