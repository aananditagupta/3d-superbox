#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){
    ofSetBackgroundColor(0);
    ofEnableDepthTest();
    ofEnableLighting();
    ofEnableNormalizedTexCoords();
    light.setPointLight();
    light.setDiffuseColor(ofFloatColor(1.0, 1.0, 1.0));
    light.setAmbientColor(ofFloatColor(0.3, 0.3, 0.3));
    light.setPosition(ofGetWidth()*.5,ofGetHeight()*.5,500);
    
    blockSize = 70;
}

//--------------------------------------------------------------
void ofApp::update(){

}

//--------------------------------------------------------------
void ofApp::draw(){
    light.enable();
    //sbox.draw();
    
//    for (SuperBox& b : blocks){
//      b.draw();
//    }
    
    for (SuperBox* b : blocks){
        b->draw();
    }
    
    
    //tree.draw();
    marker.draw();
    light.disable();
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){
    if (key == 'b'){
        SuperBox* b = new SuperBox{marker.getXPos(),
                marker.getYPos(),
                marker.getZPos(),
                blockSize, blockSize, blockSize};
        b->setTexture(blockTexture);
        blocks.push_back(b);
    }
    if (key == 't'){
        TreeBlock* t = new TreeBlock{marker.getXPos(),
                marker.getYPos(),
                marker.getZPos(),
                blockSize, blockSize, blockSize};
        blocks.push_back(t);
    }
    if (key == 'a'){
           Ball* a = new Ball{marker.getXPos(),
                   marker.getYPos(),
                   marker.getZPos(),
                   blockSize, blockSize, blockSize};
           blocks.push_back(a);
       }
    if (key == 'f'){
        Flower* f = new Flower{marker.getXPos(),
                marker.getYPos(),
                marker.getZPos(),
                blockSize, blockSize, blockSize};
        blocks.push_back(f);
    }
    if (key == 'r'){
        RockBlock* r = new RockBlock{marker.getXPos(),
                marker.getYPos(),
                marker.getZPos(),
                blockSize, blockSize, blockSize};
        r->setTexture(rockTexture);
        blocks.push_back(r);
    }
   
    marker.moveAboveBlocks(blocks);
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){
    marker.setMousePosition(x, y, ofGetWidth(), ofGetHeight());
    marker.moveAboveBlocks(blocks);
}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){
    SuperBox* b = new SuperBox{marker.getXPos(),
            marker.getYPos(),
            marker.getZPos(),
            blockSize, blockSize, blockSize};
    b->setTexture(blockTexture);
    blocks.push_back(b);
    marker.moveAboveBlocks(blocks);

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}

ofApp::~ofApp()
{
    std::cout << "destroying the blocks "<< std::endl;
    for (SuperBox* b : blocks){
        delete b; // give the memory back!
    }
}
