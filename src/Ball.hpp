//
//  Ball.hpp
//  World
//
//  Created by Aanandita Gupta on 13/12/2019.
//

#ifndef Ball_hpp
#define Ball_hpp

#include <stdio.h>


#include "superBox.h"
#include "ofMain.h"

//SuperBox is the same as block class 
class Ball : public SuperBox {
public:
    Ball(float x, float y, float z, float w, float h, float d);
    virtual void draw(); //
    void setTexture();
private:
    ofSpherePrimitive sphere;
    //ofMaterial ballMaterial;
    
    //Image source -
    //"Iridescent™" by Gulshan Singh is licensed under CC BY-NC-ND 4.0
    //https://search.creativecommons.org/photos/a9281b26-240f-46d0-9d41-a75843124801
    ofTexture texture;

};

#endif /* Ball_hpp */
