//
//  Ball.cpp
//  World
//
//  Created by Aanandita Gupta on 13/12/2019.
//

#include "Ball.hpp"


Ball::Ball(float x, float y, float z, float w, float h, float d)
    : SuperBox(x, y, z, w, h, d)
{
    sphere.setRadius(w/2);

    //ballMaterial.setDiffuseColor(ofFloatColor(0, 255, 0));
    
    std::cout << "Ball constructed " << std::endl;
}

void Ball::draw()
{
    setTexture();
    // store coordinate system
    ofPushMatrix();
    // translate coordinate 0,0,0 to the position of the block
    ofTranslate(getXPos(), getYPos(), getZPos());
    
    texture.bind();
    sphere.draw();
    texture.unbind();
    
    ofPopMatrix();
}

void Ball::setTexture()
{
    
    //Image source -
    //"Iridescent™" by Gulshan Singh is licensed under CC BY-NC-ND 4.0
    //https://search.creativecommons.org/photos/a9281b26-240f-46d0-9d41-a75843124801
    ofLoadImage(texture,"visual.jpg");
}
